package ru.dikhawk.edanebeda.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import ru.dikhawk.edanebeda.services.TimerService;

/**
 * Created by dik on 03.02.16.
 */
public class NotificationReceiver extends BroadcastReceiver {

    public static final String INTENT_FILTER_NOTIFICATION_RECEIVE = "ru.dikhawk.edanebeda.NotificationReceiver";
    private static List<NotificationReceiverListener> listeners = new ArrayList<>();
    private final String LOG_TAG = "NotificationReceiver";


    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String action = intent.getStringExtra(TimerService.ACTION);
            for (NotificationReceiverListener listener : listeners) {
                listener.action(action);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registerListener(NotificationReceiverListener listener) {
        listeners.add(listener);
    }

    public void unregisterListener(NotificationReceiverListener listener) {
        listeners.remove(listener);
    }


    public interface NotificationReceiverListener {
        public void action(String action);
    }
}
