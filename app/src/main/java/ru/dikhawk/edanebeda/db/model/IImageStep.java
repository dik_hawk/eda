package ru.dikhawk.edanebeda.db.model;

import java.io.Serializable;

/**
 * Created by dik on 29.10.15.
 */
public interface IImageStep extends Serializable {

    String getTitle();

    void setTitle(String title);

    String getDescription();

    void setDescription(String description);

    String getSrcImage();

    void setSrcImage(String url);

    String getMediumImage();

    void setMediumImage(String url);

    String getLargeImage();

    void setLargeImage(String url);
}