package ru.dikhawk.edanebeda.db.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by dik on 29.10.15.
 */
@Table(name = "images_steps")
public class ImageStepSQLite extends Model implements IImageStep {

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "src_image")
    private String srcImage;

    @Column(name = "medium_image")
    private String mediumImage;

    @Column(name = "large_image")
    private String largeImage;

    @Column(name = "steps", onDelete = Column.ForeignKeyAction.CASCADE)
    private StepSQLite step;


    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getSrcImage() {
        return srcImage;
    }

    @Override
    public void setSrcImage(String url) {
        this.srcImage = url;
    }

    @Override
    public String getMediumImage() {
        return mediumImage;
    }

    @Override
    public void setMediumImage(String url) {
        this.mediumImage = url;
    }

    @Override
    public String getLargeImage() {
        return largeImage;
    }

    @Override
    public void setLargeImage(String url) {
        this.largeImage = url;
    }

    public StepSQLite getStep() {
        return step;
    }

    public void setStep(StepSQLite step) {
        this.step = step;
    }
}