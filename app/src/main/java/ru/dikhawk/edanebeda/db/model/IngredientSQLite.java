package ru.dikhawk.edanebeda.db.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by dik on 23.09.15.
 */

@Table(name = "ingredients")
public class IngredientSQLite extends Model implements IIngredient {

    @Column(name = "ingredient_title")
    private String ingredientTitle;

    @Column(name = "quantity")
    private String quantity;

    @Column(name = "quantity_type")
    private String quantityType;

    @Column(name = "buy_it")
    private int buyIt = 0;

    @Column(name = "posts", onDelete = Column.ForeignKeyAction.CASCADE)
    private PostSQLite post;


    public IngredientSQLite() {
        super();
    }

    @Override
    public String getIngredientTitle() {
        return ingredientTitle;
    }

    @Override
    public void setIngredientTitle(String ingredientTitle) {
        this.ingredientTitle = ingredientTitle;
    }

    @Override
    public String getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Override
    public String getQuantityType() {
        return quantityType;
    }

    @Override
    public void setQuantityType(String quantityType) {
        this.quantityType = quantityType;
    }

    public PostSQLite getPost() {
        return post;
    }

    public void setPost(PostSQLite post) {
        this.post = post;
    }

    @Override
    public int isBuyIt() {
        return buyIt;
    }

    @Override
    public void setBuyIt(int buyIt) {
        this.buyIt = buyIt;
    }

    public static List<IngredientSQLite> getShoppingIngredients(long id) {
        return new Select()
                .from(IngredientSQLite.class)
                .where("buy_it > ?", BUY_IT_NO)
                .and("posts = ?", id)
                .execute();
    }
}
