package ru.dikhawk.edanebeda.db.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dik on 05.10.15.
 */
public interface IStep extends Serializable {
    String getStepText();

    void setStepText(String stepText);

    String getStepTime();

    void setStepTime(String stepTime);

    List<IImageStep> getStepImages();

    void setStepImages(List<IImageStep> stepImages);
}
