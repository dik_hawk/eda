package ru.dikhawk.edanebeda.db.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by dik on 23.09.15.
 */
@Table(name = "posts")
public class PostSQLite extends Model implements IPost {

    @Column(name = "post_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private int id;

    @Column(name = "post_date")
    private String postDate;

    @Column(name = "post_title")
    private String postTitle;

    @Column(name = "post_content")
    private String postContent;

    @Column(name = "post_type")
    private String postType;

    @Column(name = "thumbnail")
    private String thumbnail;

    @Column(name = "is_favorite")
    private boolean isFavorite = false;

    @Column(name = "time")
    private String time;

    private List<IngredientSQLite> ingredients;

    private List<StepSQLite> steps;


    public PostSQLite() {
        super();
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public void setID(int id) {
        this.id = id;
    }

    @Override
    public String getPostDate() {
        return postDate;
    }

    @Override
    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    @Override
    public String getPostTitle() {
        return postTitle;
    }

    @Override
    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    @Override
    public String getPostContent() {
        return postContent;
    }

    @Override
    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    @Override
    public String getPostType() {
        return postType;
    }

    @Override
    public void setPostType(String postType) {
        this.postType = postType;
    }

    @Override
    public String getThumbnail() {
        return thumbnail;
    }

    @Override
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public boolean isFavorite() {
        return isFavorite;
    }

    @Override
    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    @Override
    public String getTime() {
        return time;
    }

    @Override
    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public List<IngredientSQLite> getIngredients() {
        return getMany(IngredientSQLite.class, "posts");
    }

    @Override
    public void setIngredients(List ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public List<StepSQLite> getSteps() {
        return getMany(StepSQLite.class, "posts");
    }

    @Override
    public String getIngredientsCount() {
        return String.valueOf(getIngredients().size());
    }

    @Override
    public void setIngredientsCount(String ingredientsCount) {

    }

    @Override
    public void setSteps(List steps) {
        this.steps = steps;
    }

    public void savePost() {
        save();
        if (ingredients != null) {
            for (IngredientSQLite ingredient : ingredients) {
                ingredient.save();
            }
        }

        if (steps != null) {
            for (StepSQLite step : steps) {
                step.saveStep();
            }
        }
    }

    public static PostSQLite getPostById(int postId) {
        return new Select().from(PostSQLite.class).where("post_id = ?", postId).executeSingle();
    }

    public static List<PostSQLite> getFavoritePosts(int offset, int limit) {
        return new Select()
                .from(PostSQLite.class)
                .where("is_favorite = ?", true)
                .limit(limit)
                .offset(offset)
                .execute();
    }

    public static List<PostSQLite> getPostWithShopList() {
        return new Select()
                .from(PostSQLite.class)
                .innerJoin(IngredientSQLite.class)
                .on("posts.Id = ingredients.posts")
                .where("ingredients.buy_it > ?", IngredientSQLite.BUY_IT_NO)
                .groupBy("posts.Id")
                .execute();
    }
}