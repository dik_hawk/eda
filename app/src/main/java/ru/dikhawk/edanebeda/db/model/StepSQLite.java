package ru.dikhawk.edanebeda.db.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.List;

/**
 * Created by dik on 23.09.15.
 */

@Table(name = "steps")
public class StepSQLite extends Model implements IStep {

    @Column(name = "step_text")
    private String stepText;

    @Column(name = "step_time")
    private String stepTime;

    private List<ImageStepSQLite> stepImages;

    @Column(name = "posts", onDelete = Column.ForeignKeyAction.CASCADE)
    private PostSQLite post;

    public StepSQLite() {
        super();
    }

    @Override
    public String getStepText() {
        return stepText;
    }

    @Override
    public void setStepText(String stepText) {
        this.stepText = stepText;
    }

    @Override
    public String getStepTime() {
        return stepTime;
    }

    @Override
    public void setStepTime(String stepTime) {
        this.stepTime = stepTime;
    }

    @Override
    public List getStepImages() {
        return getMany(ImageStepSQLite.class, "steps");
    }

    @Override
    public void setStepImages(List stepImages) {
        this.stepImages = stepImages;
    }

    public PostSQLite getPost() {
        return post;
    }

    public void setPost(PostSQLite post) {
        this.post = post;
    }

    public void saveStep() {
        save();
        for (ImageStepSQLite imageStep : stepImages) {
            imageStep.setStep(this);
            imageStep.save();
        }
    }
}
