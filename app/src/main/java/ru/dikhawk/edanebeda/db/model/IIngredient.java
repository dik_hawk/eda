package ru.dikhawk.edanebeda.db.model;

import java.io.Serializable;

/**
 * Created by dik on 05.10.15.
 */
public interface IIngredient extends Serializable {

    int BUY_IT_NO = 0;
    int BUY_IT_BUY = 1;
    int BUY_IT_BOUGHT = 2;

    String getIngredientTitle();

    void setIngredientTitle(String ingredientTitle);

    String getQuantity();

    void setQuantity(String quantity);

    String getQuantityType();

    void setQuantityType(String quantityType);

    int isBuyIt();

    void setBuyIt(int isBuyIt);
}
