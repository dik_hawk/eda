package ru.dikhawk.edanebeda.db.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dik on 04.10.15.
 */
public interface IPost extends Serializable {

    int getID();

    void setID(int id);

    String getPostDate();

    void setPostDate(String postDate);

    String getPostTitle();

    void setPostTitle(String postTitle);

    String getPostContent();

    void setPostContent(String postContent);

    String getPostType();

    void setPostType(String postType);

    String getThumbnail();

    void setThumbnail(String thumbnail);

    boolean isFavorite();

    void setIsFavorite(boolean isFavorite);

    String getTime();

    void setTime(String time);

    List getIngredients();

    void setIngredients(List<?> ingredients);

    List getSteps();

    void setSteps(List<?> steps);

    String getIngredientsCount();

    void setIngredientsCount(String ingredientsCount);
}
