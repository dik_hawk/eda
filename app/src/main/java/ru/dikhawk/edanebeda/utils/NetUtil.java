package ru.dikhawk.edanebeda.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;

import ru.dikhawk.edanebeda.fragments.NoInternetFragment;

/**
 * Created by dik on 24.02.16.
 */
public class NetUtil {

    public static boolean checkInternetConnection(FragmentActivity activity, int resourceContainer) {
        if (checkInternetConnection(activity) == false) {
            new NoInternetFragment().viewFragment(activity, resourceContainer);
            return false;
        }

        return true;
    }

    public static boolean checkInternetConnection(FragmentActivity activity, int resourceContainer, boolean isBackStack) {
        if (checkInternetConnection(activity) == false) {
            new NoInternetFragment().viewFragment(activity, resourceContainer, isBackStack);
            return false;
        }

        return true;
    }

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo == null) {
            return false;
        }

        return true;
    }
}
