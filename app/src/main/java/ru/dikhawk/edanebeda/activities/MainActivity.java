package ru.dikhawk.edanebeda.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.fragments.FavoriteRecipesFragment;
import ru.dikhawk.edanebeda.fragments.RecipeSearchResultFragment;
import ru.dikhawk.edanebeda.fragments.RecipesCategoriesListFragment;
import ru.dikhawk.edanebeda.fragments.ShoppingListFragment;
import ru.dikhawk.edanebeda.utils.NetUtil;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String SI_CUR_POSITION_MENU = "cur_position";
    private static final String SI_CUR_FRAGMENT = "cur_fragment";
    private DrawerLayout mDrawerMenu;
    private Fragment curFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        init();
        mDrawerMenu = (DrawerLayout) findViewById(R.id.drawer_menu);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            navigationView.getMenu().getItem(0).setChecked(true);
            onNavigationItemSelected(navigationView.getMenu().getItem(0));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected int getIconRes() {
        return R.mipmap.ic_menu_white;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint(getString(R.string.search_recipe));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                RecipeSearchResultFragment fragment = RecipeSearchResultFragment.newInstance(query);
                commitFragment(fragment, true);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    private void commitFragment(Fragment fragment) {
        commitFragment(fragment, false);
    }

    private void commitFragment(Fragment fragment, boolean backStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.show_in, R.anim.show_out,
                R.anim.slide_in_left, R.anim.slide_out_left);
        transaction.replace(R.id.fragment_content, fragment);

        if (backStack) {
            transaction.addToBackStack(null);
        } else { //чистим BackStack
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    onBackPressed();
                } else {
                    mDrawerMenu.openDrawer(GravityCompat.START);
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerMenu.isDrawerVisible(GravityCompat.START)) {
            mDrawerMenu.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putInt(SI_CUR_POSITION_MENU, curPositionMenu);
//        if (curFragment != null)
//            getSupportFragmentManager().putFragment(outState, SI_CUR_FRAGMENT, curFragment);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
//        curPositionMenu = savedInstanceState.getInt(SI_CUR_POSITION_MENU);
        curFragment = getSupportFragmentManager().getFragment(savedInstanceState, SI_CUR_FRAGMENT);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_recipe:
                if (NetUtil.checkInternetConnection(this, R.id.fragment_content, false)) {
                    curFragment = new RecipesCategoriesListFragment();
                    commitFragment(curFragment);
                } else {
                    curFragment = null;
                }
                break;
            case R.id.nav_favorite:
                curFragment =
                        new FavoriteRecipesFragment();
                commitFragment(curFragment);
                break;
            case R.id.nav_shopping_list:
                curFragment =
                        new ShoppingListFragment();
                commitFragment(curFragment);
                break;
            case R.id.nav_settings:
//                mDrawerListMenu.setItemChecked(curPositionMenu, true);
//                position = curPositionMenu;
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }

        mDrawerMenu.closeDrawer(GravityCompat.START);

        return true;
    }
}
