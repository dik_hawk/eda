package ru.dikhawk.edanebeda.activities;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.octo.android.robospice.SpiceManager;

import ru.dikhawk.edanebeda.receivers.NotificationReceiver;
import ru.dikhawk.edanebeda.services.RetrofitSpiceService;

public abstract class BaseActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    private Toolbar toolbar;
    private SpiceManager spiceManager = new SpiceManager(RetrofitSpiceService.class);
    private static NotificationReceiver nr = new NotificationReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResource());
//        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
//        if (toolbar != null) {
//            setSupportActionBar(toolbar);
//        }

        registerReceiver(nr, new IntentFilter(NotificationReceiver.INTENT_FILTER_NOTIFICATION_RECEIVE));

        displayHomeUp();

        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    public static NotificationReceiver getNotificationReceiver() {
        return nr;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(nr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public SpiceManager getSpiceManager() {
        return spiceManager;
    }

    protected abstract int getLayoutResource();

    protected abstract int getIconRes();

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onBackStackChanged() {
        displayHomeUp();
    }

    public void displayHomeUp() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            boolean canBack = getSupportFragmentManager().getBackStackEntryCount() > 0;
            getSupportActionBar().setDisplayHomeAsUpEnabled(canBack);
            if (!canBack) {
                toolbar.setNavigationIcon(getIconRes());
            }
        }
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
        setSupportActionBar(toolbar);
        displayHomeUp();
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }
}
