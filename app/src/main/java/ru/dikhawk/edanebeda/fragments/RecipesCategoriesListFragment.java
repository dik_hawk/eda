package ru.dikhawk.edanebeda.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.rey.material.widget.ProgressView;

import java.util.ArrayList;
import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.activities.BaseActivity;
import ru.dikhawk.edanebeda.adapters.CategoriesRecipesListAdapter;
import ru.dikhawk.edanebeda.api.IEdaCategoriesRecipe;
import ru.dikhawk.edanebeda.api.IEdaPost;
import ru.dikhawk.edanebeda.api.model.EdaCategoriesRecipe;
import ru.dikhawk.edanebeda.utils.NetUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipesCategoriesListFragment extends Fragment implements View.OnClickListener {

    private static final String SI_CATEGORY_LIST = "category_list";
    private RecyclerView mCategoriesRecipesList;
    private final String LOG_TAG = "CategoriesRecipesList";
    private CategoriesRecipesListAdapter adapter;
    private List<EdaCategoriesRecipe> categoryList;
    private ProgressView mProgressView;

    public RecipesCategoriesListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories_recipes_list, container, false);

        ((BaseActivity) getActivity()).setToolbar((Toolbar) view.findViewById(R.id.main_toolbar));
        mCategoriesRecipesList = (RecyclerView) view.findViewById(R.id.categories_recipes_list);
        mProgressView = (ProgressView) view.findViewById(R.id.progress_view);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),
                getActivity().getResources().getInteger(R.integer.count_column_category));
        mCategoriesRecipesList.setLayoutManager(layoutManager);

        if (savedInstanceState != null)
            categoryList = (List<EdaCategoriesRecipe>) savedInstanceState.getSerializable(SI_CATEGORY_LIST);

        adapter = new CategoriesRecipesListAdapter(
                getActivity(),
                R.layout.categories_recipe_item,
                new ArrayList<EdaCategoriesRecipe>(), this);

        if (categoryList != null) {
            loadCategories();
        }
        mCategoriesRecipesList.setHasFixedSize(true);
        mCategoriesRecipesList.setAdapter(adapter);
        getActivity().setTitle(R.string.toolbar_title_categories_recipes);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (categoryList == null) {
            loadCategories();
        }
    }

    @Override
    public void onClick(View v) {

        int position = mCategoriesRecipesList.getChildLayoutPosition(v);

        if (NetUtil.checkInternetConnection(getActivity(), R.id.fragment_content)) {
            EdaCategoriesRecipe categoriesRecipe = categoryList.get(position);
            RecipesListFragment recipesListFragment =
                    RecipesListFragment.newInstance(IEdaPost.POST_TYPE_RECIPES,
                            categoriesRecipe.getCategoryCode(), categoriesRecipe.getName());
            commitFragment(recipesListFragment);
        }
    }

    private void commitFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right,
                R.anim.slide_in_left, R.anim.slide_out_left);
        transaction.replace(R.id.fragment_content, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void loadCategories() {
        if (categoryList == null) {
            mProgressView.setVisibility(View.VISIBLE);
            ((BaseActivity) getActivity()).getSpiceManager().execute(
                    new RetrofitSpiceRequest<EdaCategoriesRecipe.List, IEdaCategoriesRecipe>
                            (EdaCategoriesRecipe.List.class, IEdaCategoriesRecipe.class) {
                        @Override
                        public EdaCategoriesRecipe.List loadDataFromNetwork() throws Exception {
                            return getService().getCategoriesRecipe();
                        }
                    },
                    "request", DurationInMillis.ONE_MINUTE, new RequestListener<EdaCategoriesRecipe.List>() {
                        @Override
                        public void onRequestFailure(SpiceException spiceException) {
                            Log.e(LOG_TAG, spiceException.getMessage());
                            mProgressView.setVisibility(View.GONE);
                        }

                        @Override
                        public void onRequestSuccess(EdaCategoriesRecipe.List list) {
                            categoryList = list;
                            addList(categoryList);
                            mProgressView.setVisibility(View.GONE);
                        }
                    });
        } else {
            addList(categoryList);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (categoryList != null)
            outState.putSerializable(SI_CATEGORY_LIST, (ArrayList) categoryList);
    }

    private void addList(List<EdaCategoriesRecipe> list) {
        adapter.addList(list);
        adapter.notifyDataSetChanged();
    }
}