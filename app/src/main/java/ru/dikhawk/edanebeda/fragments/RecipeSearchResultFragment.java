package ru.dikhawk.edanebeda.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.rey.material.widget.ProgressView;

import java.util.ArrayList;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.activities.BaseActivity;
import ru.dikhawk.edanebeda.adapters.RecipeSearchAdapter;
import ru.dikhawk.edanebeda.api.IEdaPost;
import ru.dikhawk.edanebeda.api.model.EdaPost;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeSearchResultFragment extends Fragment implements View.OnClickListener {

    private static final String ARGS_SEARCH_QUERY = "search_query";
    private static final String LOG_TAG = "RecipeSearch";
    private int page = 0;
    private int PER_PAGE = 6;
    private boolean isLoad = false;
    private ProgressView mProgressDialog;
    private RecyclerView mSearchList;
    private RecipeSearchAdapter mAdapter;
    private TextView mEmptyResult;
    private GridLayoutManager mLayoutManager;
    private String querySearch;

    public RecipeSearchResultFragment() {
        // Required empty public constructor
    }

    public static RecipeSearchResultFragment newInstance(String query) {
        RecipeSearchResultFragment fragment = new RecipeSearchResultFragment();
        Bundle args = new Bundle();

        args.putString(ARGS_SEARCH_QUERY, query);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_find, container, false);

        ((BaseActivity) getActivity()).setToolbar((Toolbar) view.findViewById(R.id.main_toolbar));

        mSearchList = (RecyclerView) view.findViewById(R.id.search_list);
        mEmptyResult = (TextView) view.findViewById(R.id.empty_result);
        mProgressDialog = (ProgressView) view.findViewById(R.id.progress_view);

        mLayoutManager = new GridLayoutManager(getActivity(),
                getActivity().getResources().getInteger(R.integer.count_column_category));
        mSearchList.setLayoutManager(mLayoutManager);

        if (mAdapter == null) {
            mAdapter = new RecipeSearchAdapter(
                    getActivity().getApplicationContext(),
                    R.layout.recipe_item,
                    new ArrayList(), this);
            loadSearchRecipe(++page);
        }

        mSearchList.setAdapter(mAdapter);
        getActivity().setTitle(getString(R.string.search_title) + ": " + querySearch);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mSearchList.addOnScrollListener(scrollListener);
    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = recyclerView.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

            boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
            boolean isFull = ((RecipeSearchAdapter) mSearchList.getAdapter()).isFull();

            if (loadMore == true && !isFull && isLoad == true) {
                loadSearchRecipe(++page);
                isLoad = false;
            }
        }
    };

    private void loadSearchRecipe(final int page) {
        mProgressDialog.setVisibility(View.VISIBLE);
        querySearch = getArguments().getString(ARGS_SEARCH_QUERY);
        ((BaseActivity) getActivity()).getSpiceManager().execute(
                new RetrofitSpiceRequest<EdaPost.EdaPostList, IEdaPost>(
                        EdaPost.EdaPostList.class, IEdaPost.class) {
                    @Override
                    public EdaPost.EdaPostList loadDataFromNetwork() throws Exception {
                        return getService().getPostsByTitle(querySearch, IEdaPost.POST_TYPE_RECIPES,
                                page, PER_PAGE, IEdaPost.FORMAT_DATE_LITE);
                    }
                },
                "request_recipe_list", 1,
                new RequestListener<EdaPost.EdaPostList>() {
                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        mSearchList.setVisibility(View.GONE);
                        mEmptyResult.setVisibility(View.VISIBLE);
                        mProgressDialog.setVisibility(View.GONE);
                    }

                    @Override
                    public void onRequestSuccess(EdaPost.EdaPostList edaPosts) {
                        mAdapter.setFull(edaPosts.size() < PER_PAGE ? true : false);
                        mAdapter.addList(edaPosts);
                        mAdapter.notifyDataSetChanged();
                        isLoad = true;

                        if (edaPosts.size() == 0) {
                            mSearchList.setVisibility(View.GONE);
                            mEmptyResult.setVisibility(View.VISIBLE);
                        }
                        mProgressDialog.setVisibility(View.GONE);
                    }
                });
    }

    private void commitFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right,
                R.anim.slide_in_left, R.anim.slide_out_left);
        transaction.replace(R.id.fragment_content, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onClick(View v) {
        int position = mSearchList.getChildLayoutPosition(v);
        EdaPost edaPostLite = (EdaPost) mAdapter.getItem(position);
        RecipeFragment recipeFragment = RecipeFragment.newInstance(edaPostLite.getID(),
                edaPostLite.getPostTitle(), position);
        commitFragment(recipeFragment);
    }
}
