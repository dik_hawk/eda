package ru.dikhawk.edanebeda.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoInternetFragment extends Fragment {


    private Toolbar mMainToolbar;

    public NoInternetFragment() {
        // Required empty public constructor
    }

    public void viewFragment(FragmentActivity activity, int resourceContainer) {
        commitFragment(activity, resourceContainer, true);
    }

    public void viewFragment(FragmentActivity activity, int resourceContainer, boolean isBackStack) {
        commitFragment(activity, resourceContainer, isBackStack);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_no_internet, container, false);
        mMainToolbar = (Toolbar) view.findViewById(R.id.main_toolbar);
        ((BaseActivity) getActivity()).setToolbar(mMainToolbar);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMainToolbar.setTitle(R.string.error);
    }

    private void commitFragment(FragmentActivity activity, int resource, boolean isBackStack) {
        FragmentManager fm = activity.getSupportFragmentManager();

        if (fm.findFragmentByTag("no_internet_fragment") == null) {
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.setCustomAnimations(R.anim.show_in, R.anim.show_out,
                    R.anim.slide_in_left, R.anim.slide_out_left);
            transaction.replace(resource, new NoInternetFragment(), "no_internet_fragment");
            if (isBackStack) transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}
