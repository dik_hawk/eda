package ru.dikhawk.edanebeda.fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.util.Constants;

import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.activities.BaseActivity;
import ru.dikhawk.edanebeda.db.model.IImageStep;
import ru.dikhawk.edanebeda.db.model.IStep;
import ru.dikhawk.edanebeda.receivers.NotificationReceiver;
import ru.dikhawk.edanebeda.services.TimerService;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeStepFragment extends Fragment {


    private static final String ARGS_STEP = "step_map";
    private ImageView mImage;
    private TextView mStepText;
    private FloatingActionButton mTimer;

    public RecipeStepFragment() {
        // Required empty public constructor
    }

    public static RecipeStepFragment newInstance(IStep step) {
        RecipeStepFragment fragment = new RecipeStepFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARGS_STEP, step);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_step, container, false);
        final IStep step = (IStep) getArguments().getSerializable(ARGS_STEP);

        mImage = (ImageView) view.findViewById(R.id.image);
        mStepText = (TextView) view.findViewById(R.id.step_text);
        mTimer = (FloatingActionButton) view.findViewById(R.id.timer);

        mTimer.hide();

        if (!step.getStepTime().equals("")) {
            mTimer.show();
            mTimer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IStep step = (IStep) getArguments().getSerializable(ARGS_STEP);
                    int timeMls = Integer.valueOf(step.getStepTime()) * 60 * 1000;
                    Intent intent = new Intent(getActivity(), TimerService.class);
                    intent.putExtra(TimerService.ACTION, TimerService.ACTION_ADD_NOTIFICATION);
                    intent.putExtra(TimerService.ARG_TIME_MLS, timeMls);
                    intent.putExtra(TimerService.ARG_NOTIFICATION_ID, (int) System.currentTimeMillis());
                    intent.putExtra(TimerService.ARG_TEXT, step.getStepText());
                    getActivity().startService(intent);
                }
            });
        }

        setView();

        return view;
    }

    private NotificationReceiver.NotificationReceiverListener receiverListener =
            new NotificationReceiver.NotificationReceiverListener() {
                @Override
                public void action(String action) {
                    final IStep step = (IStep) getArguments().getSerializable(ARGS_STEP);
                    if (action.equals(TimerService.ACTION_TIMER_WAS_RUNNING)) {
                        if (!step.getStepTime().equals("")) {
                            final int timeMls = Integer.valueOf(step.getStepTime()) * 60 * 1000;
                            new AlertDialog.Builder(getActivity())
                                    .setMessage(getActivity().getString(R.string.dlg_reset_timer))
                                    .setPositiveButton(getActivity().getString(R.string.btn_reset),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent intent = new Intent(getActivity(),
                                                            TimerService.class);
                                                    intent.putExtra(TimerService.ACTION,
                                                            TimerService.ACTION_RESET_TIMER);
                                                    intent.putExtra(TimerService.ARG_TIME_MLS, timeMls);
                                                    intent.putExtra(TimerService.ARG_NOTIFICATION_ID, (int) System.currentTimeMillis());
                                                    intent.putExtra(TimerService.ARG_TEXT, step.getStepText());
                                                    getActivity().startService(intent);
                                                }
                                            })
                                    .setNegativeButton(getActivity().getString(R.string.cancel), null)
                                    .show();
                        }
                    }
                }
            };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        NotificationReceiver nr = BaseActivity.getNotificationReceiver();
        if (isVisibleToUser) {
            nr.registerListener(receiverListener);
        } else {
            nr.unregisterListener(receiverListener);
        }
    }

    private void setView() {
        IStep step = (IStep) getArguments().getSerializable(ARGS_STEP);
        AQuery aQuery = new AQuery(getActivity().getApplicationContext());
        List<IImageStep> stepImage = null;
        String stepText = "";
        String stepTime = "";

        if (step != null) {
            stepImage = step.getStepImages();
            stepText = step.getStepText();
            stepTime = step.getStepTime();
        }

        if (!stepImage.isEmpty()) {
            aQuery.id(mImage).image(
                    stepImage.get(0).getSrcImage(), true, true, 300, AQuery.GONE, null, Constants.FADE_IN);
        } else {
            mImage.setVisibility(View.GONE);
        }
        mStepText.setText(Html.fromHtml(stepText));
    }
}
