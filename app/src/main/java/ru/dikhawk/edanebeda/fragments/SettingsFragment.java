package ru.dikhawk.edanebeda.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import ru.dikhawk.edanebeda.R;


public class SettingsFragment extends PreferenceFragment {


    private static final String DEVELOPER_EMAIL = "dikhawk@gmail.com";
    private Preference mSendMailDeveloper;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        mSendMailDeveloper = findPreference("send_email_developer");
        mSendMailDeveloper.setOnPreferenceClickListener(mailListener());
    }

    private Preference.OnPreferenceClickListener mailListener() {
        return new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", DEVELOPER_EMAIL, null));
                intent.putExtra(Intent.EXTRA_EMAIL, DEVELOPER_EMAIL);
                intent.putExtra(Intent.EXTRA_SUBJECT, "EdaNebedaApp feedback: ");
                startActivity(Intent.createChooser(intent, getString(R.string.send_mail)));
                return false;
            }
        };
    }
}
