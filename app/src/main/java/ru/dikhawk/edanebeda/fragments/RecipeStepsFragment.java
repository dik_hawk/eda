package ru.dikhawk.edanebeda.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.db.model.IPost;
import ru.dikhawk.edanebeda.db.model.IStep;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeStepsFragment extends Fragment {


    private static String ARGS_POST = "steps_recipe";
    private LinearLayout mStepsListContainer;

    public RecipeStepsFragment() {
        // Required empty public constructor
    }

    public static RecipeStepsFragment newInstance(IPost post) {
        RecipeStepsFragment fragment = new RecipeStepsFragment();
        Bundle args = new Bundle();

        args.putSerializable(ARGS_POST, post);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_steps, container, false);

        mStepsListContainer = (LinearLayout) view.findViewById(R.id.steps_list_container);
        IPost post = null;
        try {
            post = (IPost) getArguments().getSerializable(ARGS_POST);
        } catch (Exception e) {
            e.printStackTrace();
        }

        addStepList(post);

        return view;
    }

    private void addStepList(final IPost post) {
        final List list = post.getSteps();

        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                final IStep step = (IStep) list.get(i);
                final int stepNumber = i + 1;
                final int position = i;
                boolean isVisible = step.getStepImages().size() != 0 ? true : false;

                View item = getStepView(String.valueOf(stepNumber), step.getStepText(), isVisible);
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        commitFragment(R.id.fragment_content,
                                RecipeStepPagerFragment.newInstance(list, position));
                    }
                });

                mStepsListContainer.addView(item);
            }
        }
    }

    private View getStepView(String stepNumber, String stepText, boolean isVisible) {
        LayoutInflater ltInflater = getActivity().getLayoutInflater();
        View item = ltInflater.inflate(R.layout.step_item, mStepsListContainer, false);
        TextView stepNumberView = (TextView) item.findViewById(R.id.step_number);
        TextView stepTextView = (TextView) item.findViewById(R.id.step_text);
        ImageView imageIcon = (ImageView) item.findViewById(R.id.image_icon);

        stepNumberView.setText(stepNumber + ") ");
        stepTextView.setText(Html.fromHtml(stepText));
        imageIcon.setVisibility(isVisible ? View.VISIBLE : View.GONE);

        return item;
    }

    private void commitFragment(int resource, Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right,
                R.anim.slide_in_left, R.anim.slide_out_left);
        transaction.replace(resource, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
