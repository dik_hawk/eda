package ru.dikhawk.edanebeda.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.activities.BaseActivity;
import ru.dikhawk.edanebeda.adapters.StepsPagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeStepPagerFragment extends Fragment {

    private ViewPager mStepsPager;
    private StepsPagerAdapter mAdapter;
    private static final String ARG_STEPS_LIST = "steps_list";
    private static final String ARGS_POSITION = "position";

    public RecipeStepPagerFragment() {
        // Required empty public constructor
    }

    public static RecipeStepPagerFragment newInstance(List steps, int position) {
        RecipeStepPagerFragment fragment = new RecipeStepPagerFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(ARG_STEPS_LIST, new ArrayList(steps));
        args.putInt(ARGS_POSITION, position);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_step_pager, container, false);

        ((BaseActivity) getActivity()).setToolbar((Toolbar) view.findViewById(R.id.main_toolbar));
        mStepsPager = (ViewPager) view.findViewById(R.id.steps_pager);

        List steps = getArguments().getStringArrayList(ARG_STEPS_LIST);
        int position = getArguments().getInt(ARGS_POSITION);
        FragmentManager fm = getChildFragmentManager();
        mAdapter = new StepsPagerAdapter(getActivity(), fm, steps);

        mStepsPager.setAdapter(mAdapter);
        mStepsPager.setCurrentItem(position);

        return view;
    }
}
