package ru.dikhawk.edanebeda.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.activities.BaseActivity;
import ru.dikhawk.edanebeda.adapters.RecipesListAdapter;
import ru.dikhawk.edanebeda.db.model.IPost;
import ru.dikhawk.edanebeda.db.model.PostSQLite;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteRecipesFragment extends Fragment implements View.OnClickListener {

    private static final String SI_FAVORITE_RECIPE = "favorite_recipe";
    private static final String SI_PAGE = "page";
    private RecyclerView mContentList;
    private RecipesListAdapter mAdapter;
    private int page = 0;
    private final int PER_PAGE = 6;
    private List<IPost> mFavoriteList = new ArrayList();
    private View mNoticeEmptyList;
    private GridLayoutManager mLayoutManager;

    public FavoriteRecipesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite_recipes, container, false);

        ((BaseActivity) getActivity()).setToolbar((Toolbar) view.findViewById(R.id.main_toolbar));
        mContentList = (RecyclerView) view.findViewById(R.id.content_list);
        mNoticeEmptyList = view.findViewById(R.id.notice_empty_list);

        mLayoutManager = new GridLayoutManager(getActivity(),
                getActivity().getResources().getInteger(R.integer.count_column_category));
        mContentList.setLayoutManager(mLayoutManager);

        if (savedInstanceState != null) {
            mFavoriteList = (ArrayList)savedInstanceState.getSerializable(SI_FAVORITE_RECIPE);
            page = savedInstanceState.getInt(SI_PAGE);
        }

        mAdapter = new RecipesListAdapter(
                getActivity().getApplicationContext(),
                R.layout.recipe_item,
                mFavoriteList, this);
        if (mFavoriteList.isEmpty())
            loadRecipe(page++);

        if (mAdapter.getCount() == 0) {
            mNoticeEmptyList.setVisibility(View.VISIBLE);
        } else {
            mNoticeEmptyList.setVisibility(View.GONE);
        }

        mContentList.setAdapter(mAdapter);

        getActivity().setTitle(R.string.toolbar_title_favorite_recipe);

        return view;
    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = recyclerView.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

            boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
            boolean isFull = ((RecipesListAdapter) mContentList.getAdapter()).isFull();

            if (loadMore == true && !isFull) {
                loadRecipe(page++);
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mContentList.addOnScrollListener(scrollListener);
    }

    private void loadRecipe(final int page) {
        List<PostSQLite> list = PostSQLite.getFavoritePosts(page * PER_PAGE, PER_PAGE);
        addList(list);
    }

    private void addList(List<PostSQLite> list) {
        mAdapter.setFull(list.size() < PER_PAGE ? true : false);
        mAdapter.addList(list);
        mAdapter.notifyDataSetChanged();
    }

    private void initOnRecipeClickListener(RecipeFragment recipeFragment) {
        recipeFragment.setOnRecipeClickListener(new RecipeFragment.OnRecipeClickListener() {
            @Override
            public void onClickFavorite(int position, boolean isSelected) {
                if (isSelected) {
                    mAdapter.clearList();
                    page = 0;
                    mAdapter.setFull(false);
                } else {
                    mAdapter.deleteItem(position);
                }
            }
        });
    }

    private void commitFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right,
                R.anim.slide_in_left, R.anim.slide_out_left);
        transaction.replace(R.id.fragment_content, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SI_PAGE, page);
        outState.putSerializable(SI_FAVORITE_RECIPE, (ArrayList) mFavoriteList);
    }

    @Override
    public void onClick(View v) {
        int position = mContentList.getChildLayoutPosition(v);
        IPost post = mAdapter.getItem(position);
        RecipeFragment recipeFragment = RecipeFragment.newInstance(post.getID(),
                post.getPostTitle(), position);
        initOnRecipeClickListener(recipeFragment);
        commitFragment(recipeFragment);
    }
}
