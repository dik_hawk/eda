package ru.dikhawk.edanebeda.fragments;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.activities.BaseActivity;
import ru.dikhawk.edanebeda.adapters.ShoppingListAdapter;
import ru.dikhawk.edanebeda.db.model.IngredientSQLite;
import ru.dikhawk.edanebeda.db.model.PostSQLite;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShoppingListFragment extends Fragment {

    private RecyclerView mShoppingList;
    private ShoppingListAdapter mShoppingAdapter;
    private boolean isDeleteTip = true;
    private StaggeredGridLayoutManager mLayoutManager;
    private View mNoticeEmptyList;

    public ShoppingListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping_list, container, false);

        ((BaseActivity) getActivity()).setToolbar((Toolbar) view.findViewById(R.id.main_toolbar));
        mShoppingList = (RecyclerView) view.findViewById(R.id.shop_it_list);
        mNoticeEmptyList = view.findViewById(R.id.notice_empty_list);

        mLayoutManager = new StaggeredGridLayoutManager(
                getResources().getInteger(R.integer.count_column_shopping_list), 1);

        mShoppingList.setLayoutManager(mLayoutManager);
        mShoppingAdapter = new ShoppingListAdapter(
                getActivity(), R.layout.shopping_item, new ArrayList());
        mShoppingList.setAdapter(mShoppingAdapter);

        fetchShopList();
        getActivity().setTitle(R.string.toolbar_title_shop_list);

        return view;
    }

    private void fetchShopList() {
        List<PostSQLite> list = PostSQLite.getPostWithShopList();
        if (list.isEmpty()) {
            mNoticeEmptyList.setVisibility(View.VISIBLE);
        } else {
            mNoticeEmptyList.setVisibility(View.GONE);
            mShoppingAdapter.addList(list);
            mShoppingAdapter.notifyDataSetChanged();

            mShoppingAdapter.setOnItemListener(new ShoppingListAdapter.OnItemListener() {
                @Override
                public void onCheckedIngredientItem(boolean isChecked, IngredientSQLite ingredient) {
                    if (isChecked) {
                        ingredient.setBuyIt(IngredientSQLite.BUY_IT_BOUGHT);
                    } else {
                        ingredient.setBuyIt(IngredientSQLite.BUY_IT_BUY);
                    }
                    ingredient.save();
                }

                @Override
                public void onClickDeleteButton(final View view, final PostSQLite post, int position) {
                    initDeleteButton(view, post, position);
                    int count = mShoppingList.getAdapter().getItemCount();
                    if (count == 0) {
                        mNoticeEmptyList.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

    private void initDeleteButton(final View view, final PostSQLite post, final int position) {

        mShoppingAdapter.deleteItem(post);
        Snackbar snackbar = Snackbar.make(getView(), R.string.tip_is_delete_from_shopping_list,
                Snackbar.LENGTH_LONG)
                .setAction(R.string.undo, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isDeleteTip = false;
                        mShoppingAdapter.restoreLastDeleteItem();
                        int count = mShoppingList.getAdapter().getItemCount();
                        if (count == 0) {
                            mNoticeEmptyList.setVisibility(View.VISIBLE);
                        } else {
                            mNoticeEmptyList.setVisibility(View.GONE);
                        }
                    }
                });
        snackbar.getView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {

            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                if (isDeleteTip) {
                    List<IngredientSQLite> ingredients =
                            IngredientSQLite.getShoppingIngredients(post.getId());

                    for (IngredientSQLite ingredient : ingredients) {
                        ingredient.setBuyIt(IngredientSQLite.BUY_IT_NO);
                        ingredient.save();
                    }

                    isDeleteTip = true;
                }
            }
        });

        snackbar.show();
    }
}
