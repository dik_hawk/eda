package ru.dikhawk.edanebeda.fragments;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.adapters.IngredientsListAdapter;
import ru.dikhawk.edanebeda.api.model.EdaPost;
import ru.dikhawk.edanebeda.db.model.IIngredient;
import ru.dikhawk.edanebeda.db.model.IPost;
import ru.dikhawk.edanebeda.db.model.IngredientSQLite;
import ru.dikhawk.edanebeda.db.model.PostSQLite;
import ru.dikhawk.edanebeda.view.ExpandableHeightGridView;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeIngredientsFragment extends Fragment implements
        IngredientsListAdapter.OnFavoriteCheckboxChanged {

    private static final String ARG_POST = "post";
    private IPost mPost;
    private ExpandableHeightGridView mIngredientsList;
    private IngredientsListAdapter mAdapter;
    private CheckBox mAddAll;
    private CompoundButton.OnCheckedChangeListener addAllListener =
            new CompoundButton.OnCheckedChangeListener(){
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    allCheck(isChecked);
                }
            };

    public RecipeIngredientsFragment() {
        // Required empty public constructor
    }

    public static RecipeIngredientsFragment newInstance(IPost post) {
        RecipeIngredientsFragment fragment = new RecipeIngredientsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_POST, post);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_ingredients, container, false);
        mIngredientsList = (ExpandableHeightGridView) view.findViewById(R.id.ingredients_list);
        mAddAll = (CheckBox) view.findViewById(R.id.add_all);

        mPost = (IPost) getArguments().getSerializable(ARG_POST);
        if (mAdapter == null) {
            mAdapter = new IngredientsListAdapter(getActivity().getApplicationContext(),
                    R.layout.ingredient_item, mPost, this);
        }

        mIngredientsList.setAdapter(mAdapter);
        mIngredientsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox cb = (CheckBox) view.findViewById(R.id.add_ingredient);
                if (cb.isChecked()) {
                    cb.setChecked(false);
                } else {
                    cb.setChecked(true);
                }
            }
        });
        mAddAll.setChecked(mAdapter.isAllBuy());
        mAddAll.setOnCheckedChangeListener(addAllListener);

        return view;
    }

    private void allCheck(boolean isChecked) {
        for (int i = 0; i < mIngredientsList.getChildCount(); i++) {
            View view = mIngredientsList.getChildAt(i);
            CheckBox addIngredient = (CheckBox) view.findViewById(R.id.add_ingredient);
            if (addIngredient.isChecked() != isChecked) {
                addIngredient.setChecked(isChecked);
            }
        }
        mAddAll.setChecked(isChecked);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked, int position) {
        PostSQLite postSQLite = PostSQLite.getPostById(mPost.getID());

        if (postSQLite == null) {
            EdaPost.toPostSQLite((EdaPost) mPost).savePost();
            postSQLite = PostSQLite.getPostById(mPost.getID());
        }

        IngredientSQLite ingredient = postSQLite.getIngredients().get(position);

        if (isChecked) {
            ingredient.setBuyIt(IIngredient.BUY_IT_BUY);
            showSnackBar(getString(R.string.snack_bar_msg_add_ingredient_to_shop_list));
        } else {
            ingredient.setBuyIt(IIngredient.BUY_IT_NO);
            showSnackBar(getString(R.string.snack_bar_msg_remove_ingredient_to_shop_list));
        }

        ingredient.save();
        mAddAll.setOnCheckedChangeListener(null);
        mAddAll.setChecked(mAdapter.isAllBuy());
        mAddAll.setOnCheckedChangeListener(addAllListener);
    }

    private void showSnackBar(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
    }
}
