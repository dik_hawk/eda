package ru.dikhawk.edanebeda.fragments;


import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.util.Constants;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.rey.material.widget.ProgressView;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.activities.BaseActivity;
import ru.dikhawk.edanebeda.api.IEdaPost;
import ru.dikhawk.edanebeda.api.model.EdaPost;
import ru.dikhawk.edanebeda.db.model.IPost;
import ru.dikhawk.edanebeda.db.model.PostSQLite;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeFragment extends Fragment {

    private static final String LOG_TAG = "Recipe";
    private static final String SI_RECIPE_POST = "recipe_post";
    private static final String ARGS_RECIPE_TITLE = "recipe_title";
    private static final String ARGS_RECIPE_POSITION = "recipe_position";
    private static final String ARGS_POST_ID = "post_id";
    private ImageView mThumbnail;
    private FloatingActionButton mFavorite;
    private TextView mTime;
    private NestedScrollView mContainer;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private IPost mPost;
    private int mRecipeId;
    private OnRecipeClickListener onRecipeClickListener;
    private boolean isFavorite = false;
    private ProgressView mProgressView;
    private AppBarLayout mAppBar;
    private boolean isExpanded = true;

    public RecipeFragment() {
        // Required empty public constructor
    }

    public static RecipeFragment newInstance(int recipeId, String recipeTitle, int position) {
        RecipeFragment recipeFragment = new RecipeFragment();
        Bundle args = new Bundle();
        args.putInt(ARGS_POST_ID, recipeId);
        args.putString(ARGS_RECIPE_TITLE, recipeTitle);
        args.putInt(ARGS_RECIPE_POSITION, position);
        recipeFragment.setArguments(args);

        return recipeFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe, container, false);

        if (savedInstanceState != null) {
            isExpanded = false;
        }

        ((BaseActivity) getActivity()).setToolbar((Toolbar) view.findViewById(R.id.main_toolbar));
        mProgressView = (ProgressView) view.findViewById(R.id.progress_view);
        mThumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        mFavorite = (FloatingActionButton) view.findViewById(R.id.btn_favorite);
        mTime = (TextView) view.findViewById(R.id.time);
        mContainer = (NestedScrollView) view.findViewById(R.id.sw_container);
        mAppBar = (AppBarLayout) view.findViewById(R.id.main_appbar);

        mRecipeId = getArguments().getInt(ARGS_POST_ID);

        mCollapsingToolbarLayout =
                (CollapsingToolbarLayout) view.findViewById(R.id.main_collapsing);

        mCollapsingToolbarLayout.setTitle(getArguments().getString(ARGS_RECIPE_TITLE));
        getActivity().setTitle(getArguments().getString(ARGS_RECIPE_TITLE));

        if (savedInstanceState != null) {
            mPost = (IPost) savedInstanceState.getSerializable(SI_RECIPE_POST);
        } else {
            //Загружаем из SQLite рецепт
            PostSQLite post = PostSQLite.getPostById(mRecipeId);
            if (post != null) {
                mPost = PostSQLite.getPostById(mRecipeId);
            }
        }

        mAppBar.setExpanded(false, false);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private View.OnClickListener favoriteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PostSQLite postSQLite = PostSQLite.getPostById(mRecipeId);
            isFavorite = isFavorite ? false : true;

            if (isFavorite) {
                if (postSQLite == null) {
                    postSQLite = EdaPost.toPostSQLite((EdaPost) mPost);
                }
                postSQLite.setIsFavorite(true);
                postSQLite.savePost();
                Snackbar.make(mContainer,
                        R.string.snack_bar_msg_add_recipe, Snackbar.LENGTH_LONG).show();
                mFavorite.setImageResource(R.drawable.ic_favorite_push);
            } else if (!isFavorite) {
                postSQLite.setIsFavorite(false);
                postSQLite.savePost();
                Snackbar.make(mContainer,
                        R.string.snack_bar_msg_delete_recipe, Snackbar.LENGTH_LONG).show();
                mFavorite.setImageResource(R.drawable.ic_favorite_unpush);
            }

            if (onRecipeClickListener != null) {
                int position = getArguments().getInt(ARGS_RECIPE_POSITION);
                onRecipeClickListener.onClickFavorite(position, isFavorite);
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        loadRecipe();
    }

    public void setOnRecipeClickListener(OnRecipeClickListener onRecipeClickListener) {
        this.onRecipeClickListener = onRecipeClickListener;
    }

    private void loadRecipe() {
        //Если при инициализация  оказалось что такого рецепта нет в базе, то загружаем с сайта
        if (mPost == null) {
            mContainer.setVisibility(View.GONE);
            mProgressView.setVisibility(View.VISIBLE);
            ((BaseActivity) getActivity()).getSpiceManager().execute(
                    new RetrofitSpiceRequest<IPost, IEdaPost>(IPost.class, IEdaPost.class) {
                        @Override
                        public IPost loadDataFromNetwork() throws Exception {
                            return getService().getPost(mRecipeId);
                        }
                    }, "request", DurationInMillis.ONE_MINUTE,
                    new RequestListener<IPost>() {
                        @Override
                        public void onRequestFailure(SpiceException spiceException) {
                            Log.e(LOG_TAG, spiceException.getMessage());
                            mProgressView.setVisibility(View.GONE);
                        }

                        @Override
                        public void onRequestSuccess(IPost post) {
                            mPost = post;
                            setViews(mPost);
                            mContainer.setVisibility(View.VISIBLE);
                            mProgressView.setVisibility(View.GONE);
                        }
                    });
        } else {
            setViews(mPost);
        }
    }

    private void addIngredientsList(IPost post) {
        if (!post.getIngredients().isEmpty())
            commitFragment(R.id.ingredients_container, RecipeIngredientsFragment.newInstance(post));
    }

    private void addStepsList(IPost post) {
        if (!post.getSteps().isEmpty())
            commitFragment(R.id.steps_container, RecipeStepsFragment.newInstance(post));
    }

    private void commitFragment(final int resource, final Fragment fragment) {
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(resource, fragment);
        transaction.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }

    private void setViews(IPost post) {
        AQuery aQuery = new AQuery(getActivity());

        aQuery.id(mThumbnail).image(post.getThumbnail(), true, true, 300, 0, null, Constants.FADE_IN);
        mTime.setText(post.getTime() == null ? "" : post.getTime() + " " + getActivity().getString(R.string.min));
        isFavorite = mPost.isFavorite();
        mFavorite.setImageResource(mPost.isFavorite() ? R.drawable.ic_favorite_push : R.drawable.ic_favorite_unpush);
        addIngredientsList(post);
        addStepsList(post);
        mFavorite.setOnClickListener(favoriteListener);
        mAppBar.setExpanded(isExpanded, false);
        mAppBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                isExpanded = verticalOffset == 0;
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mPost != null)
            outState.putSerializable(SI_RECIPE_POST, mPost);
    }

    public interface OnRecipeClickListener {
        void onClickFavorite(int position, boolean isSelected);
    }
}