package ru.dikhawk.edanebeda.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.ArrayList;
import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.activities.BaseActivity;
import ru.dikhawk.edanebeda.adapters.RecipesListAdapter;
import ru.dikhawk.edanebeda.api.IEdaPost;
import ru.dikhawk.edanebeda.api.model.EdaPost;
import ru.dikhawk.edanebeda.db.model.IPost;
import ru.dikhawk.edanebeda.db.model.PostSQLite;
import ru.dikhawk.edanebeda.utils.NetUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipesListFragment extends Fragment implements View.OnClickListener {

    private static final String ARGS_POST_TYPE = "post_type";
    private static final String ARGS_CODE_CATEGORY = "category";
    private static final String SI_PAGE = "page";
    private static final String ARGS_TITLE_CATEGORY = "title_category";
    private static final String SI_ADAPTER_LIST = "adapter_list";
    private static final String SI_RV_STATE = "rv_state";
    private RecyclerView mContentList;
    private List<IPost> mRecipeList;
    private int page = 0;
    private final int PER_PAGE = 6;
    private final String LOG_TAG = "RecipesListFragment";
    private boolean isLoad = false;
    private RecipesListAdapter mAdapter;
    private View mCustomProgressView;
    private GridLayoutManager mLayoutManager;

    public RecipesListFragment() {
        // Required empty public constructor
    }

    public static RecipesListFragment newInstance(String postType, String codeCategory,
                                                  String titleCategory) {
        RecipesListFragment fragment = new RecipesListFragment();
        Bundle args = new Bundle();
        args.putString(ARGS_POST_TYPE, postType);
        args.putString(ARGS_CODE_CATEGORY, codeCategory);
        args.putString(ARGS_TITLE_CATEGORY, titleCategory);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe_list, container, false);

        ((BaseActivity) getActivity()).setToolbar((Toolbar) view.findViewById(R.id.main_toolbar));
        mCustomProgressView = view.findViewById(R.id.custom_progress_bar);
        mContentList = (RecyclerView) view.findViewById(R.id.content_list);

        mLayoutManager = new GridLayoutManager(getActivity(),
                getActivity().getResources().getInteger(R.integer.count_column_category));
        mContentList.setLayoutManager(mLayoutManager);

        if (savedInstanceState != null) {
            mRecipeList = (ArrayList) savedInstanceState.getSerializable(SI_ADAPTER_LIST);
            page = savedInstanceState.getInt(SI_PAGE);
        }

        mAdapter = new RecipesListAdapter(
                getActivity().getApplicationContext(),
                R.layout.recipe_item,
                mRecipeList != null ? mRecipeList : (mRecipeList = new ArrayList()), this);

        if (mRecipeList.isEmpty()) {
            loadRecipe(++page);
        } else {
            isLoad = true;
        }

        mContentList.setAdapter(mAdapter);
        getActivity().setTitle(getArguments().getString(ARGS_TITLE_CATEGORY));

        return view;
    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = recyclerView.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

            boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
            boolean isFull = ((RecipesListAdapter) mContentList.getAdapter()).isFull();

            if (loadMore == true && !isFull && isLoad == true) {
                loadRecipe(++page);
                isLoad = false;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mContentList.addOnScrollListener(scrollListener);
    }

    private void loadRecipe(final int page) {
        final String postType = getArguments().getString(ARGS_POST_TYPE);
        final String codeCategory = getArguments().getString(ARGS_CODE_CATEGORY);

        mCustomProgressView.setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).getSpiceManager().execute(
                new RetrofitSpiceRequest<EdaPost.EdaPostList, IEdaPost>(EdaPost.EdaPostList.class,
                        IEdaPost.class) {
                    @Override
                    public EdaPost.EdaPostList loadDataFromNetwork() throws Exception {
                        return getService().getPosts(page, PER_PAGE, postType,
                                IEdaPost.FORMAT_DATE_LITE, codeCategory);
                    }
                },
                "request_recipe_list", 1,
                new RequestListener<EdaPost.EdaPostList>() {
                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        Log.e(LOG_TAG, spiceException.getMessage());
                        mCustomProgressView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onRequestSuccess(EdaPost.EdaPostList posts) {
                        if (posts.isEmpty()) {

                        } else {
                            addList(posts);
                        }
                        isLoad = true;
                        mCustomProgressView.setVisibility(View.GONE);
                    }
                });
    }

    private void addList(EdaPost.EdaPostList list) {
        mAdapter.setFull(list.size() < PER_PAGE ? true : false);
        mAdapter.addList(list);
        mAdapter.notifyDataSetChanged();
    }

    private void commitFragment(Fragment fragment) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        if (fm == null) fm = getChildFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right,
                R.anim.slide_in_left, R.anim.slide_out_left);
        transaction.replace(R.id.fragment_content, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
        fm.executePendingTransactions();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(SI_ADAPTER_LIST, (ArrayList<IPost>) mRecipeList);
        outState.putInt(SI_PAGE, page);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mRecipeList = (ArrayList) savedInstanceState.getParcelableArrayList(SI_ADAPTER_LIST);
            page = savedInstanceState.getInt(SI_PAGE);
        }
    }

    @Override
    public void onClick(View v) {
        int position = mContentList.getChildLayoutPosition(v);
        IPost post = mAdapter.getItem(position);

        PostSQLite postLite = PostSQLite.getPostById(post.getID());
        if (!NetUtil.checkInternetConnection(getActivity()) && postLite != null) {
            RecipeFragment recipeFragment = RecipeFragment.newInstance(post.getID(),
                    post.getPostTitle(), position);
            commitFragment(recipeFragment);
        } else if (NetUtil.checkInternetConnection(getActivity(), R.id.fragment_content)) {
            RecipeFragment recipeFragment = RecipeFragment.newInstance(post.getID(),
                    post.getPostTitle(), position);
            commitFragment(recipeFragment);
        }
    }
}
