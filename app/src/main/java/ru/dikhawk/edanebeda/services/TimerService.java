package ru.dikhawk.edanebeda.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.receivers.NotificationReceiver;

/**
 * Created by dik on 02.02.16.
 */
public class TimerService extends Service {

    public static final String ARG_NOTIFICATION_ID = "arg_notification_id";
    public static final String ACTION = "action";
    public static final String ACTION_ADD_NOTIFICATION = "action_add_notification";
    public static final String ACTION_CANCEL_NOTIFICATION = "action_cancel_notification";
    public static final String ACTION_RESET_TIMER = "action_reset_timer";
    public static final String ACTION_TIMER_WAS_RUNNING = "action_timer_was_running";
    public static final String ARG_TIME_MLS = "arg_time_mls";
    public static final String ARG_TEXT = "arg_text";
    private NotificationManager nm;
    private NotificationReceiver nr;
    private CountDownTimer task = null;
    private Context context;
    private int notificationID = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        Log.i("TimerService", "isCreate");
        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        String action = "";
        String text = "";
        int timeMls = 0;

        if (intent != null) {
            action = intent.getStringExtra(ACTION);
            notificationID = intent.getIntExtra(ARG_NOTIFICATION_ID, 0);
            timeMls = intent.getIntExtra(ARG_TIME_MLS, 0);
            text = intent.getStringExtra(ARG_TEXT);
        }

        switch (action) {
            case ACTION_ADD_NOTIFICATION:
                if (task != null) {
                    Intent intentResetTimer = new Intent(context, NotificationReceiver.class);
                    intentResetTimer.putExtra(ACTION, ACTION_TIMER_WAS_RUNNING);
                    sendBroadcast(intentResetTimer);
                } else {
                    if (notificationID != 0) {
                        startTimer("", text, timeMls, startId, notificationID);
                    }
                }
                break;
            case ACTION_RESET_TIMER:
                task.cancel();
                if (notificationID != 0) {
                    startTimer("", text, timeMls, startId, notificationID);
                }
                break;
        }

        if (nr == null) {
            nr = new NotificationReceiver();
            nr.registerListener(new NotificationReceiver.NotificationReceiverListener() {
                @Override
                public void action(String action) {
                    switch (action) {
                        case ACTION_CANCEL_NOTIFICATION:
                            stopForeground(false);
                            nm.cancel(notificationID);
                            if (task != null) {
                                task.cancel();
                                task = null;
                                stopSelf();
                            }
                            break;
                    }
                }
            });
            registerReceiver(nr, new IntentFilter(NotificationReceiver.INTENT_FILTER_NOTIFICATION_RECEIVE));
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(nr);
        Log.i("TimerService", "Destroy");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startTimer(String header, final String text, int mls, final int startId, final int notifyID) {
        final DateFormat format = new SimpleDateFormat("HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        startForeground(notifyID, getNotification(notifyID, true, true, format.format(new Date(mls)), text, 0));
        task = new CountDownTimer(mls, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                nm.notify(notifyID,
                        getNotification(notifyID, true, false, format.format(new Date(millisUntilFinished)),
                                text, 0));
            }

            @Override
            public void onFinish() {
                stopForeground(true);
                nm.cancel(notifyID);
                nm.notify(notifyID, getNotification(notifyID, false, true, getString(R.string.is_ready), text, 0));
                task = null;
            }

        }.start();
    }

    private Notification getNotification(int notifyID, boolean isActionButton, boolean isSound,
                                         String title, String text, int flag) {
        Notification notification;
        Intent intentCancel = new Intent(context, NotificationReceiver.class);
        intentCancel.putExtra(ACTION, ACTION_CANCEL_NOTIFICATION);
        intentCancel.putExtra(ARG_NOTIFICATION_ID, notifyID);
        PendingIntent pIntent = PendingIntent.getBroadcast(context,
                (int) System.currentTimeMillis(), intentCancel, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(R.mipmap.ic_timer)
                .setOnlyAlertOnce(true);


        if (isActionButton) {
            builder
                    .setContentIntent(pIntent)
                    .addAction(R.mipmap.ic_cancel, getString(R.string.btn_cancel), pIntent);
        }

        if (isSound) {
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        }


        notification = builder.build();
        notification.flags |= flag;

        return notification;
    }
}
