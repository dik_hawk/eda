package ru.dikhawk.edanebeda.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.dikhawk.edanebeda.db.model.IImageStep;

/**
 * Created by dik on 29.10.15.
 */
public class EdaStepImage implements IImageStep {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("src_image")
    @Expose
    private String srcImage;

    @SerializedName("medium_image")
    @Expose
    private String mediumImage;

    @SerializedName("large_image")
    @Expose
    private String largeImage;


    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getSrcImage() {
        return srcImage;
    }

    @Override
    public void setSrcImage(String url) {
        srcImage = url;
    }

    @Override
    public String getMediumImage() {
        return mediumImage;
    }

    @Override
    public void setMediumImage(String url) {
        mediumImage = url;
    }

    @Override
    public String getLargeImage() {
        return largeImage;
    }

    @Override
    public void setLargeImage(String url) {
        largeImage = url;
    }
}