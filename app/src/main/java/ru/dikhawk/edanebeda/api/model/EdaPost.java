package ru.dikhawk.edanebeda.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import ru.dikhawk.edanebeda.db.model.IPost;
import ru.dikhawk.edanebeda.db.model.ImageStepSQLite;
import ru.dikhawk.edanebeda.db.model.IngredientSQLite;
import ru.dikhawk.edanebeda.db.model.PostSQLite;
import ru.dikhawk.edanebeda.db.model.StepSQLite;

/**
 * Created by dik on 30.07.15.
 */
public class EdaPost implements IPost {
    @SerializedName("ID")
    @Expose
    private int id;

    @SerializedName("post_author")
    @Expose
    private int postAuthor;

    @SerializedName("post_date")
    @Expose
    private String postDate;

    @SerializedName("post_date_gmt")
    @Expose
    private String postDateGMT;

    @SerializedName("post_title")
    @Expose
    private String postTitle;

    @SerializedName("post_content")
    @Expose
    private String postContent;

    @SerializedName("post_excerpt")
    @Expose
    private String postExcerpt;

    @SerializedName("post_status")
    @Expose
    private String postStatus;

    @SerializedName("comment_status")
    @Expose
    private String commentStatus;

    @SerializedName("ping_status")
    @Expose
    private String pingStatus;

    @SerializedName("post_password")
    @Expose
    private String postPassword;

    @SerializedName("post_name")
    @Expose
    private String postName;

    @SerializedName("to_ping")
    @Expose
    private String toPing;

    @SerializedName("pinged")
    @Expose
    private String pinged;

    @SerializedName("post_modified")
    @Expose
    private String postModified;

    @SerializedName("post_modified_gmt")
    @Expose
    private String postModifiedGmt;

    @SerializedName("post_content_filtered")
    @Expose
    private String postContentFiltered;

    @SerializedName("post_parent")
    @Expose
    private String postParent;

    @SerializedName("guid")
    @Expose
    private String guid;

    @SerializedName("menu_order")
    @Expose
    private int menuOrder;

    @SerializedName("post_type")
    @Expose
    private String postType;

    @SerializedName("post_mime_type")
    @Expose
    private String postMimeType;

    @SerializedName("comment_count")
    @Expose
    private int commentCount;

    @SerializedName("steps")
    @Expose
    private List<EdaStep> steps;

    @SerializedName("ingredients")
    @Expose
    private List<EdaIngredient> ingredients;

    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    @SerializedName("ingredients_count")
    @Expose
    private String ingredientsCount;

    @SerializedName("time")
    @Expose
    private String time;

    @Override
    public int getID() {
        return id;
    }

    @Override
    public void setID(int id) {
        this.id = id;
    }

    public int getPostAuthor() {
        return postAuthor;
    }

    public void setPostAuthor(int postAuthor) {
        this.postAuthor = postAuthor;
    }

    @Override
    public String getPostDate() {
        return postDate;
    }

    @Override
    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostDateGMT() {
        return postDateGMT;
    }

    public void setPostDateGMT(String postDateGMT) {
        this.postDateGMT = postDateGMT;
    }

    @Override
    public String getPostTitle() {
        return postTitle;
    }

    @Override
    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    @Override
    public String getPostContent() {
        return postContent;
    }

    @Override
    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getPostExcerpt() {
        return postExcerpt;
    }

    public void setPostExcerpt(String postExcerpt) {
        this.postExcerpt = postExcerpt;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostParent(String postParent) {
        this.postParent = postParent;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(int menuOrder) {
        this.menuOrder = menuOrder;
    }

    @Override
    public String getPostType() {
        return postType;
    }

    @Override
    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostMimeType() {
        return postMimeType;
    }

    public void setPostMimeType(String postMimeType) {
        this.postMimeType = postMimeType;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public String getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getPingStatus() {
        return pingStatus;
    }

    public void setPingStatus(String pingStatus) {
        this.pingStatus = pingStatus;
    }

    public String getPostPassword() {
        return postPassword;
    }

    public void setPostPassword(String postPassword) {
        this.postPassword = postPassword;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getToPing() {
        return toPing;
    }

    public void setToPing(String toPing) {
        this.toPing = toPing;
    }

    public String getPinged() {
        return pinged;
    }

    public void setPinged(String pinged) {
        this.pinged = pinged;
    }

    public String getPostModified() {
        return postModified;
    }

    public void setPostModified(String postModified) {
        this.postModified = postModified;
    }

    public String getPostModifiedGmt() {
        return postModifiedGmt;
    }

    public void setPostModifiedGmt(String postModifiedGmt) {
        this.postModifiedGmt = postModifiedGmt;
    }

    public String getPostContentFiltered() {
        return postContentFiltered;
    }

    public void setPostContentFiltered(String postContentFiltered) {
        this.postContentFiltered = postContentFiltered;
    }

    public String getPostParent() {
        return postParent;
    }

    @Override
    public List getSteps() {
        return steps;
    }

    @Override
    public void setSteps(List steps) {
        this.steps = steps;
    }

    @Override
    public List getIngredients() {
        return ingredients;
    }

    @Override
    public void setIngredients(List ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String getThumbnail() {
        return thumbnail;
    }

    @Override
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public boolean isFavorite() {
        return false;
    }

    @Override
    public void setIsFavorite(boolean isFavorite) {

    }

    @Override
    public String getIngredientsCount() {
        return ingredientsCount;
    }

    @Override
    public void setIngredientsCount(String ingredientsCount) {
        this.ingredientsCount = ingredientsCount;
    }

    @Override
    public String getTime() {
        return time;
    }

    @Override
    public void setTime(String time) {
        this.time = time;
    }

    public static PostSQLite toPostSQLite(EdaPost edaPost) {
        PostSQLite postSQLite = new PostSQLite();
        postSQLite.setID(edaPost.getID());
        postSQLite.setPostDate(edaPost.getPostDate());
        postSQLite.setPostTitle(edaPost.getPostTitle());
        postSQLite.setPostContent(edaPost.getPostContent());
        postSQLite.setPostType(edaPost.getPostType());
        postSQLite.setThumbnail(edaPost.getThumbnail());
        postSQLite.setTime(edaPost.getTime());

        postSQLite.setIngredients(toIngredientSQLite(edaPost.getIngredients(), postSQLite));
        postSQLite.setSteps(toStepSQLite(edaPost.getSteps(), postSQLite));

        return postSQLite;
    }

    private static List<IngredientSQLite> toIngredientSQLite(List<EdaIngredient> ingredients, PostSQLite postSQLite) {
        List<IngredientSQLite> result = new ArrayList();

        if (ingredients == null) {
            return result;
        }

        if (ingredients.isEmpty()) {
            return result;
        }

        for (EdaIngredient ingredient : ingredients) {
            IngredientSQLite ingredientSQLite = new IngredientSQLite();
            String quantity = ingredient.getQuantity();
            String ingredientTitle = ingredient.getIngredientTitle();
            String quantityType = ingredient.getQuantityType();

            ingredientSQLite.setQuantity(quantity);
            ingredientSQLite.setIngredientTitle(ingredientTitle);
            ingredientSQLite.setQuantityType(quantityType);
            ingredientSQLite.setPost(postSQLite);

            result.add(ingredientSQLite);
        }

        return result;
    }

    private static List<StepSQLite> toStepSQLite(List<EdaStep> steps, PostSQLite postSQLite) {
        List<StepSQLite> result = new ArrayList();

        if (steps == null) {
            return result;
        }

        if (steps.isEmpty()) {
            return result;
        }

        for (EdaStep step : steps) {
            StepSQLite stepSQLite = new StepSQLite();
            String stepText = step.getStepText();
            String stepTime = step.getStepTime();
            List<EdaStepImage> stepImages = step.getStepImages();

            stepSQLite.setStepText(stepText);
            stepSQLite.setPost(postSQLite);
            stepSQLite.setStepTime(stepTime);
            stepSQLite.setStepImages(toImageStepSQLite(stepImages, stepSQLite));

            result.add(stepSQLite);
        }

        return result;
    }

    private static List<ImageStepSQLite> toImageStepSQLite(
            List<EdaStepImage> stepImages, StepSQLite step) {
        List<ImageStepSQLite> result = new ArrayList<>();

        if (stepImages == null) {
            return result;
        }

        if (stepImages.isEmpty()) {
            return result;
        }

        for (EdaStepImage image : stepImages) {
            ImageStepSQLite imageStepSQLite = new ImageStepSQLite();
            imageStepSQLite.setTitle(image.getTitle());
            imageStepSQLite.setDescription(image.getDescription());
            imageStepSQLite.setSrcImage(image.getSrcImage());
            imageStepSQLite.setLargeImage(image.getLargeImage());
            imageStepSQLite.setMediumImage(image.getMediumImage());
            imageStepSQLite.setStep(step);
            result.add(imageStepSQLite);
        }

        return result;
    }

    public static class EdaPostList extends ArrayList<EdaPost> {
    }
}
