package ru.dikhawk.edanebeda.api;

import retrofit.RestAdapter;

/**
 * Created by dik on 31.07.15.
 */
public class EdaAPIFactory {
    private static final String URL_API = "http://local.test/api/";

    public static IEdaPost createEdaPost() {
        return getRestAdapter().create(IEdaPost.class);
    }

    public static IEdaCategoriesRecipe createEdaCategoriesRecipe() {
        return getRestAdapter().create(IEdaCategoriesRecipe.class);
    }

    private static RestAdapter getRestAdapter() {
        return new RestAdapter.Builder()
                .setEndpoint(URL_API).build();
    }
}
