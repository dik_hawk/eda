package ru.dikhawk.edanebeda.api;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import ru.dikhawk.edanebeda.api.model.EdaPost;

/**
 * Created by dik on 31.07.15.
 */
public interface IEdaPost {
    String POST_TYPE_RECIPES = "recipes";
    String FORMAT_DATE_LITE = "lite";

    @GET("/posts/{post_id}")
    EdaPost getPost(@Path("post_id") int postId);

    @GET("/posts")
    EdaPost.EdaPostList getPosts(@Query("page") int page,
                                 @Query("per_page") int perPage,
                                 @Query("post_type") String postType,
                                 @Query("format_data") String formatData,
                                 @Query("category") String category);

    @GET("/posts/title/{title_name}")
    void getPostsByTitle(@Path("title_name") String name,
                         @Query("post_type") String postType,
                         @Query("page") int page,
                         @Query("per_page") int perPage,
                         @Query("format_data") String formatData,
                         Callback<List<EdaPost>> response);

    @GET("/posts/title/{title_name}")
    EdaPost.EdaPostList getPostsByTitle(@Path("title_name") String name,
                                        @Query("post_type") String postType,
                                        @Query("page") int page,
                                        @Query("per_page") int perPage,
                                        @Query("format_data") String formatData);
}
