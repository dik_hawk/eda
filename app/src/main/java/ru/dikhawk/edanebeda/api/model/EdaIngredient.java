package ru.dikhawk.edanebeda.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.dikhawk.edanebeda.db.model.IIngredient;

/**
 * Created by dik on 05.10.15.
 */
public class EdaIngredient implements IIngredient {

    @SerializedName("ingredient")
    @Expose
    private String ingredientTitle;

    @SerializedName("quantity")
    @Expose
    private String quantity;

    @SerializedName("quantity_type")
    @Expose
    private String quantityType;

    @Override
    public String getIngredientTitle() {
        return ingredientTitle;
    }

    @Override
    public void setIngredientTitle(String ingredientTitle) {
        this.ingredientTitle = ingredientTitle;
    }

    @Override
    public String getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Override
    public String getQuantityType() {
        return quantityType;
    }

    @Override
    public void setQuantityType(String quantityType) {
        this.quantityType = quantityType;
    }

    @Override
    public int isBuyIt() {
        return 0;
    }

    @Override
    public void setBuyIt(int isBuyIt) {

    }
}
