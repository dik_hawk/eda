package ru.dikhawk.edanebeda.api;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import ru.dikhawk.edanebeda.api.model.EdaCategoriesRecipe;

/**
 * Created by dik on 23.08.15.
 */
public interface IEdaCategoriesRecipe {

    @GET("/categories-of-dishes")
    void getCategoriesRecipe(Callback<List<EdaCategoriesRecipe>> response);

    @GET("/categories-of-dishes")
    EdaCategoriesRecipe.List getCategoriesRecipe();
}
