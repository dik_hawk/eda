package ru.dikhawk.edanebeda.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.dikhawk.edanebeda.db.model.IStep;

/**
 * Created by dik on 05.10.15.
 */
public class EdaStep implements IStep {

    @SerializedName("step_text")
    @Expose
    private String stepText;

    @SerializedName("step_time")
    @Expose
    private String stepTime;

    @SerializedName("step_images")
    @Expose
    private List<EdaStepImage> stepImages;

    @Override
    public String getStepText() {
        return stepText;
    }

    @Override
    public void setStepText(String stepText) {
        this.stepText = stepText;
    }

    @Override
    public String getStepTime() {
        return stepTime;
    }

    @Override
    public void setStepTime(String stepTime) {
        this.stepTime = stepTime;
    }

    @Override
    public List getStepImages() {
        return stepImages;
    }

    @Override
    public void setStepImages(List stepImages) {
        this.stepImages = stepImages;
    }
}
