package ru.dikhawk.edanebeda;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.flurry.android.FlurryAgent;

/**
 * Created by dik on 06.05.15.
 */
public class MyApplication extends Application {

    private final String FLURRY_API_KEY = "N28SDRMT4GXSJ669B6H3";

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        boolean flurryStatistic = sp.getBoolean("flurry_statistic", true);

        if (flurryStatistic) {
            // configure Flurry
            FlurryAgent.setLogLevel(Log.VERBOSE);
            FlurryAgent.setLogEnabled(true);
            FlurryAgent.setLogEvents(true);
            FlurryAgent.setReportLocation(true);
            FlurryAgent.setCaptureUncaughtExceptions(true);

            // init Flurry
            FlurryAgent.init(this, FLURRY_API_KEY);
        }

        Configuration dbConfiguration = new Configuration.Builder(this).setDatabaseName(getString(R.string.db_name)).create();
        ActiveAndroid.initialize(dbConfiguration);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
