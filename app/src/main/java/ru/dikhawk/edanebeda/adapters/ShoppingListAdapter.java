package ru.dikhawk.edanebeda.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.rey.material.widget.ImageButton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.db.model.IngredientSQLite;
import ru.dikhawk.edanebeda.db.model.PostSQLite;

/**
 * Created by dik on 07.10.15.
 */
public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.ViewHolder> {
    private List<PostSQLite> list;
    private Context context;
    private int resource;
    private boolean isFull = false;
    private OnItemListener onItemListener;
    private Map<Long, List<IngredientSQLite>> allIngredients = new HashMap();
    private PostSQLite lastDeletePost;
    private int lastPostIndex;
    private List<IngredientSQLite> lastDeleteIngredientList;

    public ShoppingListAdapter(Context context, int resource, List list) {
        this.context = context;
        this.resource = resource;
        this.list = list;
    }

    public OnItemListener getOnItemListener() {
        return onItemListener;
    }

    public void setOnItemListener(OnItemListener onItemListener) {
        this.onItemListener = onItemListener;
    }

    public boolean isFull() {
        return isFull;
    }

    public void setFull(boolean isFull) {
        this.isFull = isFull;
    }

    public void addList(List list) {
        this.list.addAll(list);
    }

    public void deleteItem(PostSQLite post) {
        lastDeletePost = post;
        lastDeleteIngredientList = allIngredients.get(lastDeletePost.getId());
        lastPostIndex = list.indexOf(post);

        list.remove(post);
        allIngredients.remove(lastDeletePost.getId());
        notifyItemRemoved(lastPostIndex);
    }

    public void restoreLastDeleteItem() {
        if (lastDeletePost != null && lastDeleteIngredientList != null && lastPostIndex != -1) {
            Log.i("deleteItem position", String.valueOf(lastPostIndex));
            list.add(lastPostIndex, lastDeletePost);
            allIngredients.put(lastDeletePost.getId(), lastDeleteIngredientList);
            notifyItemInserted(lastPostIndex);
            lastDeletePost = null;
            lastDeleteIngredientList = null;
            lastPostIndex = -1;
        }

    }

    private View getIngredientItemView(final IngredientSQLite ingredient,
                                       ViewGroup viewGroup,
                                       final int ingredientPosition,
                                       final int itemPosition) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View item = inflater.inflate(R.layout.shopping_ingredient_item, viewGroup, false);
        final AppCompatCheckBox isBuyIt = (AppCompatCheckBox) item.findViewById(R.id.is_buy_it);
        final TextView titleIngredient = (TextView) item.findViewById(R.id.title_ingredient);

        isBuyIt.setOnCheckedChangeListener(null);
        isBuyIt.setChecked(ingredient.isBuyIt() == IngredientSQLite.BUY_IT_BUY ? false :
                ingredient.isBuyIt() == IngredientSQLite.BUY_IT_BOUGHT ? true : false);
        titleIngredient.setText(
                ingredient.getIngredientTitle() + " "
                        + ingredient.getQuantity() + " "
                        + ingredient.getQuantityType());

        if (ingredient.isBuyIt() == IngredientSQLite.BUY_IT_BUY) {
            titleIngredient.setPaintFlags(
                    titleIngredient.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        } else if (ingredient.isBuyIt() == IngredientSQLite.BUY_IT_BOUGHT) {
            titleIngredient.setPaintFlags(
                    titleIngredient.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        if (onItemListener != null) {
            isBuyIt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final boolean isChecked = isBuyIt.isChecked();

                    if (isChecked) {
                        titleIngredient.setPaintFlags(
                                titleIngredient.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    } else {
                        titleIngredient.setPaintFlags(
                                titleIngredient.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    }

                    onItemListener.onCheckedIngredientItem(isChecked, ingredient);
                }
            });
        }

        return item;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(resource, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        AQuery aQuery = new AQuery(context);
        final PostSQLite post = list.get(position);
        List<IngredientSQLite> ingredients = allIngredients.get(post.getId());
        if (ingredients == null) {
            ingredients = IngredientSQLite.getShoppingIngredients(post.getId());
            allIngredients.put(post.getId(), ingredients);
        }

        holder.recipeTitle.setText(post.getPostTitle());

        if (list != null) {
            //осовобждаем контайнер для перерисовки вьюшки
            holder.ingredientsContainer.removeAllViews();
            for (int i = 0; i < ingredients.size(); i++) {
                IngredientSQLite ingredient = ingredients.get(i);
                View item = getIngredientItemView(ingredient, holder.ingredientsContainer, i, position);
                holder.ingredientsContainer.addView(item, i);
            }
        }

        if (onItemListener != null) {
            final View view = holder.itemView;
            holder.deleteTip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemListener.onClickDeleteButton(view, post, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView recipeTitle;
        public LinearLayout ingredientsContainer;
        public ImageButton deleteTip;

        public ViewHolder(View itemView) {
            super(itemView);
            recipeTitle = (TextView) itemView.findViewById(R.id.recipe_title);
            ingredientsContainer = (LinearLayout) itemView.findViewById(R.id.shopping_ingredients_container);
            deleteTip = (ImageButton) itemView.findViewById(R.id.delete_tip);
        }
    }

    public interface OnItemListener {
        void onCheckedIngredientItem(boolean isChecked, IngredientSQLite ingredient);

        void onClickDeleteButton(View view, PostSQLite post, int position);
    }
}