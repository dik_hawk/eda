package ru.dikhawk.edanebeda.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.util.Constants;

import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.api.model.EdaCategoriesRecipe;

/**
 * Created by dik on 23.08.15.
 */
public class CategoriesRecipesListAdapter extends RecyclerView.Adapter<CategoriesRecipesListAdapter.ViewHolder> {

    private Context context;
    private int resource;
    private List<EdaCategoriesRecipe> list;
    private boolean isFull = false;
    private View.OnClickListener listener;

    public CategoriesRecipesListAdapter(Context context, int resource,
                                        List<EdaCategoriesRecipe> list, View.OnClickListener listener) {
        this.context = context;
        this.resource = resource;
        this.list = list;
        this.listener = listener;
    }

    public boolean isFull() {
        return isFull;
    }

    public void setFull(boolean isFull) {
        this.isFull = isFull;
    }

    public void addList(List<EdaCategoriesRecipe> list) {
        this.list.addAll(list);
    }

    public List<EdaCategoriesRecipe> getList() {
        return list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        view.setOnClickListener(listener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoriesRecipesListAdapter.ViewHolder holder, int position) {
        AQuery aQuery = new AQuery(context);
        EdaCategoriesRecipe categoriesRecipe = list.get(position);

        if (categoriesRecipe.getThumbnail() != null) {
            aQuery.id(holder.thumbnail).image(categoriesRecipe.getThumbnail(), true, true, 300, 0,
                    null, Constants.FADE_IN);
        }
        holder.categoryName.setText(categoriesRecipe.getName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public TextView categoryName;

        public ViewHolder(View itemView) {
            super(itemView);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            categoryName = (TextView) itemView.findViewById(R.id.category_name);
        }
    }
}
