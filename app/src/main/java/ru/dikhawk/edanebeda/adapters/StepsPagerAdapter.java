package ru.dikhawk.edanebeda.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.db.model.IStep;
import ru.dikhawk.edanebeda.fragments.RecipeStepFragment;

/**
 * Created by dik on 30.08.15.
 */
public class StepsPagerAdapter extends FragmentStatePagerAdapter {

    private final List<?> list;
    private final Context context;

    public StepsPagerAdapter(Context context, FragmentManager fm, List steps) {
        super(fm);
        this.context = context;
        list = steps;
    }

    @Override
    public Fragment getItem(int position) {
        return RecipeStepFragment.newInstance((IStep) list.get(position));
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getString(R.string.step) + " " + String.valueOf(position + 1);
    }
}
