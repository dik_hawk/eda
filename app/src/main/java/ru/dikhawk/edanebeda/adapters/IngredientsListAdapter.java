package ru.dikhawk.edanebeda.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.db.model.IIngredient;
import ru.dikhawk.edanebeda.db.model.IPost;
import ru.dikhawk.edanebeda.db.model.IngredientSQLite;
import ru.dikhawk.edanebeda.db.model.PostSQLite;

/**
 * Created by dik on 27.08.15.
 */
public class IngredientsListAdapter extends BaseAdapter {


    private final Context context;
    private final int resource;
    private final List<IIngredient> list;
    private final IPost post;
    private IngredientsListAdapter.OnFavoriteCheckboxChanged listener;

    public IngredientsListAdapter(Context context, int resource, IPost post,
                                  IngredientsListAdapter.OnFavoriteCheckboxChanged listener) {
        this.context = context;
        this.resource = resource;
        this.list = post.getIngredients();
        this.post = post;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public boolean isAllBuy(){
        boolean isAllBuy = true;

        for (IIngredient ingredient : list) {
            if (ingredient.isBuyIt() != IIngredient.BUY_IT_BUY) {
                isAllBuy = false;
                break;
            }
        }

        return isAllBuy;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(resource, parent, false);
            holder = new ViewHolder();

            holder.addIngredient = (CheckBox) rowView.findViewById(R.id.add_ingredient);
            holder.ingredientTitle = (TextView) rowView.findViewById(R.id.ingredient_title);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }


        PostSQLite postSQLite = PostSQLite.getPostById(post.getID());
        IIngredient ingredient = (IIngredient) list.get(position);

        String quantity = "";
        String ingredientTitle = "";
        String quantityType = "";

        quantity = ingredient.getQuantity();
        ingredientTitle = ingredient.getIngredientTitle();
        quantityType = ingredient.getQuantityType();

        holder.ingredientTitle.setText(ingredientTitle + " " + quantity + " " + quantityType);

        if (postSQLite != null) {
            holder.addIngredient.setOnCheckedChangeListener(null);
            int buyIt = postSQLite.getIngredients().get(position).isBuyIt();
            holder.addIngredient.setChecked(buyIt == IngredientSQLite.BUY_IT_BUY);
        }

        holder.addIngredient.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listener.onCheckedChanged(buttonView, isChecked, position);
            }
        });

        return rowView;
    }

    private static class ViewHolder {
        public CheckBox addIngredient;
        public TextView ingredientTitle;
    }

    public interface OnFavoriteCheckboxChanged {
        void onCheckedChanged(CompoundButton buttonView, boolean isChecked, int position);

    }
}
