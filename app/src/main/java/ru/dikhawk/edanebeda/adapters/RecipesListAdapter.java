package ru.dikhawk.edanebeda.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.util.Constants;

import java.io.Serializable;
import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.db.model.IPost;

/**
 * Created by dik on 05.08.15.
 */
public class RecipesListAdapter extends RecyclerView.Adapter<RecipesListAdapter.ViewHolder> {

    private Context context;
    private List<IPost> list;
    private int resource;
    private boolean isFull = false;
    private View.OnClickListener listener;

    public RecipesListAdapter(Context context, int resource, List list, View.OnClickListener listener) {
        this.context = context;
        this.resource = resource;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        view.setOnClickListener(listener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecipesListAdapter.ViewHolder holder, int position) {
        AQuery aQuery = new AQuery(context);
        IPost post = ((List<IPost>) list).get(position);

        if (post.getThumbnail() != null) {
            aQuery.id(holder.recipePic).image(post.getThumbnail(), true, true, 200, 0, null, Constants.FADE_IN);
        }
        holder.titleRecipe.setText(post.getPostTitle());
        holder.ingredientsCount
                .setText(post.getIngredientsCount() + " "
                        + context.getString(R.string.ingredients_little));
        holder.time.setText(String.valueOf(post.getTime()) + " " + context.getString(R.string.min));
    }

    public IPost getItem(int position) {
        return list.get(position);
    }

    public boolean isFull() {
        return isFull;
    }

    public void setFull(boolean isFull) {
        this.isFull = isFull;
    }

    public void addList(List list) {
        this.list.addAll(list);
    }

    public void deleteItem(int position) {
        list.remove(position);
    }

    public void clearList() {
        list.clear();
    }

    public int getSize() {
        return list.size();
    }

    public int getCount() {
        return list.size();
    }

    public List<IPost> getList() {
        return list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView recipePic;
        public TextView titleRecipe;
        public TextView time;
        public TextView ingredientsCount;

        public ViewHolder(View itemView) {
            super(itemView);
            recipePic = (ImageView) itemView.findViewById(R.id.recipe_pic);
            titleRecipe = (TextView) itemView.findViewById(R.id.title_recipe);
            time = (TextView) itemView.findViewById(R.id.time);
            ingredientsCount = (TextView) itemView.findViewById(R.id.ingredients_count);
        }
    }
}
