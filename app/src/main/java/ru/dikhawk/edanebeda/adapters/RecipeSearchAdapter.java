package ru.dikhawk.edanebeda.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.util.Constants;

import java.util.List;

import ru.dikhawk.edanebeda.R;
import ru.dikhawk.edanebeda.db.model.IPost;

/**
 * Created by dik on 13.09.15.
 */
public class RecipeSearchAdapter extends RecyclerView.Adapter<RecipeSearchAdapter.ViewHolder> {

    private List<IPost> list;
    private Context context;
    private int resource;
    private boolean isFull = false;
    private View.OnClickListener listener;

    public RecipeSearchAdapter(Context context, int resource, List<IPost> list, View.OnClickListener listener) {
        this.context = context;
        this.resource = resource;
        this.list = list;
        this.listener = listener;
    }

    public IPost getItem(int position) {
        return list.get(position);
    }

    public boolean isFull() {
        return isFull;
    }

    public void setFull(boolean isFull) {
        this.isFull = isFull;
    }

    public void addList(List list) {
        this.list.addAll(list);
    }

    public void deleteItem(int position) {
        list.remove(position);
    }

    public void clearList() {
        list.clear();
    }

    public int getSize() {
        return list.size();
    }

    public int getCount() {
        return list.size();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        view.setOnClickListener(listener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AQuery aQuery = new AQuery(context);
        IPost post = ((List<IPost>) list).get(position);

        if (post.getThumbnail() != null) {
            aQuery.id(holder.recipePic).image(post.getThumbnail(), true, true, 200, 0, null, Constants.FADE_IN);
        }
        holder.titleRecipe.setText(post.getPostTitle());
        holder.ingredientsCount
                .setText(post.getIngredientsCount() + " "
                        + context.getString(R.string.ingredients_little));
        holder.time.setText(String.valueOf(post.getTime()) + " " + context.getString(R.string.min));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView recipePic;
        public TextView titleRecipe;
        public TextView time;
        public TextView ingredientsCount;

        public ViewHolder(View itemView) {
            super(itemView);
            recipePic = (ImageView) itemView.findViewById(R.id.recipe_pic);
            titleRecipe = (TextView) itemView.findViewById(R.id.title_recipe);
            time = (TextView) itemView.findViewById(R.id.time);
            ingredientsCount = (TextView) itemView.findViewById(R.id.ingredients_count);
        }
    }

//    public boolean isFull() {
//        return isFull;
//    }
//
//    public void setFull(boolean isFull) {
//        this.isFull = isFull;
//    }
//
//    public void addList(List<EdaPost> list) {
//        recipeList.addAll(list);
//    }
//
//    @Override
//    public int getCount() {
//        return recipeList.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return recipeList.get(position);
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return 0;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder;
//        AQuery aQuery = new AQuery(context);
//        View rowView = convertView;
//
//        if (rowView == null) {
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            rowView = inflater.inflate(resource, parent, false);
//            holder = new ViewHolder();
//
//            holder.recipePic = (ImageView) rowView.findViewById(R.id.recipe_pic);
//            holder.titleRecipe = (TextView) rowView.findViewById(R.id.title_recipe);
//            holder.time = (TextView) rowView.findViewById(R.id.time);
//            holder.ingredientsCount = (TextView) rowView.findViewById(R.id.ingredients_count);
//
//            rowView.setTag(holder);
//        } else {
//            holder = (ViewHolder) rowView.getTag();
//        }
//
//        IPost post = recipeList.get(position);
//        if (post.getThumbnail() != null) {
//            aQuery.id(holder.recipePic).image(post.getThumbnail(), true, true, 300, 0, null, Constants.FADE_IN);
//        }
//        holder.titleRecipe.setText(post.getPostTitle());
//        holder.ingredientsCount.setText(String.valueOf(post.getIngredientsCount()) + " " + context.getString(R.string.ingredients_little));
//        holder.time.setText(String.valueOf(post.getTime()) + " " + context.getString(R.string.min));
//
//        return rowView;
//    }
//
//    private static class ViewHolder {
//        public ImageView recipePic;
//        public TextView titleRecipe;
//        public TextView time;
//        public TextView ingredientsCount;
//    }
}
